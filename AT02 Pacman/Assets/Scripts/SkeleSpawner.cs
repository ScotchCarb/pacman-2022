using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SkeleSpawner : MonoBehaviour
{

    //bounds for skeleton spawn area
    [SerializeField] private Bounds skeleSpawnBounds;

    //game objects for Skeles
    [SerializeField] private Transform clyde;
    [SerializeField] private Transform blinky;
    [SerializeField] private Transform inky;
    [SerializeField] private Transform pinky;

    //number of sets of skeletons to spawn in
    public int numSkels;


    private void Awake()
    {
        //for each set of skeletons to spawn in, instantiate 1x Clyde, 1x Blinky, 1x Inky, 1x Pinky within bounds
        for(int i = 0; i < numSkels; i++)
        {
            int ran = Random.Range(0, 2);
            Instantiate(clyde, new Vector3((transform.position.x + ran), (transform.position.y), (transform.position.z + ran)), Quaternion.identity);
            Instantiate(blinky, new Vector3((transform.position.x + ran), (transform.position.y), (transform.position.z + ran)), Quaternion.identity);
            Instantiate(inky, new Vector3((transform.position.x + ran), (transform.position.y), (transform.position.z + ran)), Quaternion.identity);
            Instantiate(pinky, new Vector3((transform.position.x + ran), (transform.position.y), (transform.position.z + ran)), Quaternion.identity);
        }
    }
}
