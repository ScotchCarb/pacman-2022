using System.Collections;
using System.Collections.Generic;
using UnityEngine;

//Script by Ian R Bell, last updated 23/3/2022
public class WorldMap : MonoBehaviour
{
    //reference to map object
    public GameObject map;
    //reference to player object
    public GameObject player;
    //reference to the Pacman script
    public Pacman pacmanScript;

    //reference to the PacMan icon in UI & skeleton icons
    public GameObject pacSprite;
    

    

    private void Awake()
    {
        //make sure that player has a Pacman script and assign it to the variable
        player.TryGetComponent(out pacmanScript);
    }

    void Update()
    {
        //detect player input and toggle the map on/off
        if (Input.GetButtonDown("map"))
        {
            Toggle();
        }


    }

    void Toggle()
    {
        //if the map is currently active when script is called
        if(map.activeSelf == true)
        {
            //set the map to inactive
            map.SetActive(false);
            //enable the pacman script so player can move
            pacmanScript.enabled = true;
        }
        else
        {
            //set the map to active
            map.SetActive(true);
            //disavle the pacman script so polayer can't move
            pacmanScript.enabled = false;
        }
    }
}
