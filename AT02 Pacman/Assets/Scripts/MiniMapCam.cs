using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MiniMapCam : MonoBehaviour
{
    //create reference to player transform
    public Transform player;

    //lateupdate function to get called after Update and FixedUpdate
    private void LateUpdate()
    {
        //create a vector3 for the player's current position
        Vector3 newPosition = player.transform.position;
        //set the y of the newPosition to equal the current y so the camera maintains its height
        newPosition.y = transform.position.y;
        //set the minimap camera transform to equal the player's transform
        transform.position = newPosition;

    }
}
