using System.Collections;
using System.Collections.Generic;
using UnityEngine;


//Script by Ian R Bell, last updated 23/3/2022
public class DoorScript : MonoBehaviour
{
    //variable for holding the animator on the door
    private Animator anim;

    private void Awake()
    {
        //make sure that the door has an animator on it
        TryGetComponent(out anim);
    }

    private void OnTriggerEnter(Collider other)
    {
        //check if object has Ghost tag
        if(other.CompareTag("Ghost"))
        {
            //open door            
            Open();
        }        
    }

    private void OnTriggerExit(Collider other)
    {
        //check if object has Ghost tag
        if (other.CompareTag("Ghost") || (other.CompareTag("Player")))
        {
            //close door
            //replace with the Open() function
            Close();
        }
    }
    public void Open()
    {
        //open the door when the player interacts with it
        //anim.SetBool functinality from above needs to be added        
        anim.SetBool("openDoor", true);
        
    }

    public void Close()
    {
        anim.SetBool("openDoor", false);
    }

    public string Prompt()
    {
        Debug.Log("You Stand Before A Door");
        string doorString = "You Stand Before A Door";
        return doorString;
    }
}
