using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LevelGenerator : MonoBehaviour
{
    //array of prefab rooms to spawn
    [SerializeField] private Transform[] rooms;

    //array of roomspawner objects
    private GameObject[] roomSpawners;

    private void Awake()
    {
        //find all roomspawners and assign to roomspawner array
        roomSpawners = GameObject.FindGameObjectsWithTag("RoomSpawner");

        //for each roomspawner spawn a prefab room from the list
        for(int i = 0; i < roomSpawners.Length; i++)
        {
            int x = Random.Range(0, rooms.Length);
            Instantiate(rooms[x], new Vector3(roomSpawners[i].transform.position.x, roomSpawners[i].transform.position.y, roomSpawners[i].transform.position.z), Quaternion.identity);
        }
    }


    void Update()
    {
        
    }
}
