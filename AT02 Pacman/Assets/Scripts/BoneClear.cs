using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BoneClear : MonoBehaviour
{
    private float timer = 0f;

    private void Update()
    {
        if(timer >= 0)
        {
            timer += Time.deltaTime;
        }
        if(timer >= 0.55f)
        {
            Destroy(gameObject);
        }
    }

}
