using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;
using System.Runtime.Serialization.Formatters.Binary;
using System.IO;

public abstract class Ghost : MonoBehaviour
{
    //Serialized variables
    [SerializeField] private Material fleeMaterial;
    [SerializeField] private Material respawnMaterial;

    //prefab for rigid body bones
    [SerializeField] private GameObject looseBones;

    //Auto-properties
    public Pacman Target { get; private set; }
    public NavMeshAgent Agent { get; private set; }
    public MeshRenderer GhostRenderer { get; private set; }
    public Material DefaultMaterial { get; private set; }
    public GhostState DefaultState { get; protected set; }
    public GhostState_Flee FleeState { get; private set; }
    public GhostState_ClydeFlee ClydeFleeState { get; private set; }
    public GhostState_Respawn RespawnState { get; private set; }
    public GhostState CurrentState { get; private set; }

    public AudioSource audioSource { get; private set; }

    public SkinnedMeshRenderer HeartRenderer;
    public SkinnedMeshRenderer lEyeRenderer;
    public SkinnedMeshRenderer rEyeRenderer;
    public Material DefaultHeartMat;

    public AudioClip[] aggBark;
    public AudioClip[] fleeBark;
    public AudioClip[] dieBark;
    public AudioClip[] killBark;

    public SkinnedMeshRenderer[] boneRenderer;

    private int diffPreset;

    /// <summary>
    /// Creates necessary references.
    /// </summary>
    protected virtual void Awake()
    {
        DefaultHeartMat = HeartRenderer.material;

        //Get mesh renderer reference
        TryGetComponent(out MeshRenderer renderer);
        if (renderer != null)
        {
            GhostRenderer = renderer;
            DefaultMaterial = GhostRenderer.material;
        }
        else
        {
            Debug.LogError($"Ghost: {gameObject.name} must have a Mesh Renderer!");
        }
        TryGetComponent(out AudioSource source);
        if (source != null)
        {
            audioSource = source;
           
        }
        else
        {
            Debug.LogError($"Ghost: {gameObject.name} must have a Audio Source!");
        }
        //Get nav mesh agent reference
        TryGetComponent(out NavMeshAgent agent);
        if(agent != null)
        {
            Agent = agent;
        }
        else
        {
            Debug.LogError($"Ghost: {gameObject.name} must have a NavMesh Agent!");
        }
        //Initialize states
        FleeState = new GhostState_Flee(this, fleeMaterial);
        RespawnState = new GhostState_Respawn(this, respawnMaterial);
        ClydeFleeState = new GhostState_ClydeFlee(this, fleeMaterial);
        //Find target reference
        GameObject.FindGameObjectWithTag("Player").TryGetComponent(out Pacman pacman);
        if (pacman != null)
        {
            Target = pacman;
        }
        else
        {
            Debug.LogError($"Ghost: Pacman must be tagged as 'Player'.");
        }

        //check game settings ini for difficulty level & adjust:
        //turn speed on navmesh agent
        //skeleton speed on navmesh agent
        LoadDiff();
        SetSpeeds(Agent, diffPreset);

      

    }

    /// <summary>
    /// Assign delegate/events and set initial state.
    /// </summary>
    protected virtual void Start()
    {
        GameManager.Instance.Event_PickUpPowerPellet += TriggerFleeState;
        GameManager.Instance.Event_EndPowerUp += TriggerDefaultState;
        GameManager.Instance.Event_GameVictory += delegate { SetState(new GhostState_Idle(this)); };
        GameManager.Instance.Delegate_GameOver += delegate { SetState(new GhostState_Idle(this)); };
        SetState(DefaultState);
    }

    /// <summary>
    /// Frame by frame functionality.
    /// </summary>
    protected virtual void Update()
    {
        //Update currently running state
        if (CurrentState != null)
        {
            CurrentState.OnUpdate();
        }
    }

    /// <summary>
    /// Exits current state to enter new state.
    /// </summary>
    /// <param name="state"></param>
    /// 

    public void LoadDiff()
    {
        if(File.Exists(Application.persistentDataPath + "/GameData.dat"))
        {
            BinaryFormatter gameData = new BinaryFormatter();
            FileStream file = File.Open(Application.persistentDataPath + "/GameData.dat", FileMode.Open);
            GameData data = (GameData)gameData.Deserialize(file);
            file.Close();
            diffPreset = data.SaveDiff;
        }
        else
        {
            diffPreset = 1;
        }
    }

    public void SetSpeeds(NavMeshAgent agent, int diff)
    {
        switch (diff)
        {           
            case 1:
                agent.angularSpeed = 90;
                agent.speed = 1;
                break;
            case 2:
                agent.angularSpeed = 120;
                agent.speed = 2;
                break;
            case 3:
                agent.angularSpeed = 150;
                agent.speed = 3;
                break;
            case 4:
                agent.angularSpeed = 180;
                agent.speed = 4;
                break;
            case 5:
                agent.angularSpeed = 300;
                agent.speed = 6;
                break;
             default:
                agent.angularSpeed = 90;
                agent.speed = 1;
                break;
        }
    }

    public void SetState(GhostState state)
    {
        if (state != CurrentState)
        {
            //Leave current state
            if (CurrentState != null)
            {
                CurrentState.OnExit();
            }
            //Enter new state
            CurrentState = state;
            CurrentState.OnEnter();
        }
    }

    /// <summary>
    /// Assigned to Power Pellet pickup event
    /// </summary>
    private void TriggerFleeState()
    {
        if (CurrentState.GetType() != typeof(GhostState_Respawn))
        {
            SetState(FleeState);
        }
    }

    private void TriggerClydeFleeState()
    {
        if (CurrentState.GetType() != typeof(GhostState_Respawn))
        {
            SetState(ClydeFleeState);
        }
    }

    /// <summary>
    /// Assigned to Power Pellet end event
    /// </summary>
    protected void TriggerDefaultState()
    {
        if (CurrentState != RespawnState)
        {
            SetState(DefaultState);
        }
    }

    public void SpawnBones()
    {
        Instantiate(looseBones, new Vector3(transform.position.x, (transform.position.y - 1), transform.position.z), Quaternion.identity);
    }
}

/// <summary>
/// Interface for creating FSM state classes.
/// </summary>
public interface IGhostState
{
    void OnEnter(); //Called when state is first entered
    void OnUpdate(); //Called every frame state is active
    void OnExit(); //Called when the state is exited
}

/// <summary>
/// Abstract class for defining Ghost states.
/// </summary>
public abstract class GhostState : IGhostState
{
    protected Ghost Instance { get; private set; }

    public GhostState(Ghost instance)
    {
        Instance = instance;
    }

    public virtual void OnEnter()
    {
    }

    public virtual void OnUpdate()
    {
    }

    public virtual void OnExit()
    {
    }

}

public class GhostState_Idle : GhostState
{
    public GhostState_Idle(Ghost instance) : base(instance)
    {
    }

    public override void OnEnter()
    {
        if (Instance.GhostRenderer.material != Instance.DefaultMaterial)
        {
            Instance.GhostRenderer.material = Instance.DefaultMaterial;
        }
        Instance.Agent.isStopped = true;
    }

    public override void OnExit()
    {
        Instance.Agent.isStopped = false;
    }
}

public class GhostState_Chase : GhostState
{
    private Transform target;

    public GhostState_Chase(Ghost instance) : base(instance)
    {
    }

    public override void OnEnter()
    {
        if (Instance.GhostRenderer.material != Instance.DefaultMaterial)
        {
            Instance.GhostRenderer.material = Instance.DefaultMaterial;
            Instance.HeartRenderer.material = Instance.DefaultHeartMat;
            Instance.lEyeRenderer.material = Instance.DefaultHeartMat;
            Instance.rEyeRenderer.material = Instance.DefaultHeartMat;
        }
        if (Instance.Target != null)
        {
            target = Instance.Target.transform;
        }
        else
        {
            Instance.SetState(new GhostState_Idle(Instance));
        }
    }

    public override void OnUpdate()
    {
        if (Random.Range(0, 100000) < 20)
        {
            int x = Random.Range(0, (Instance.aggBark.Length));            
            Instance.audioSource.PlayOneShot(Instance.aggBark[x]);
        }

        if (Vector3.Distance(Instance.transform.position, target.position) > Instance.Agent.stoppingDistance)
        {
            Instance.Agent.SetDestination(target.position);
        }
        else
        {
            if(Instance.Target.Die() == true)
            {
                Instance.SetState(new GhostState_Idle(Instance));
            }
        }
    }
}

public class GhostState_Flank : GhostState
{
    private Vector3 offset;
    private Transform target;

    public GhostState_Flank(Ghost instance, Vector3 targetOffset) : base(instance)
    {
        offset = targetOffset;
    }

    public override void OnEnter()
    {
       
        
        if (Instance.GhostRenderer.material != Instance.DefaultMaterial)
        {
            Instance.GhostRenderer.material = Instance.DefaultMaterial;
            Instance.HeartRenderer.material = Instance.DefaultHeartMat;
            Instance.lEyeRenderer.material = Instance.DefaultHeartMat;
            Instance.rEyeRenderer.material = Instance.DefaultHeartMat;
        }
        if (Instance.Target != null)
        {
            target = Instance.Target.transform;
        }
        else
        {
            Instance.SetState(new GhostState_Idle(Instance));
        }
    }

    public override void OnUpdate()
    {
        if (Random.Range(0, 100000) < 20)
        {
            int x = Random.Range(0, (Instance.aggBark.Length));
            Instance.audioSource.PlayOneShot(Instance.aggBark[x]);
        }
        if (Vector3.Distance(Instance.transform.position, target.position) > Instance.Agent.stoppingDistance)
        {
            Instance.Agent.SetDestination(target.position + offset);
            Debug.DrawLine(Instance.transform.position, target.position + offset, Color.magenta);
        }
        else
        {
            if (Instance.Target.Die() == true)
            {
                Instance.SetState(new GhostState_Idle(Instance));
            }
        }
    }
}

public class GhostState_Flee : GhostState
{
    private Material fleeMaterial;
    private Transform target;

    public GhostState_Flee(Ghost instance) : base(instance)
    {
    }

    public GhostState_Flee(Ghost instance, Material fleeMat) : base(instance)
    {
        fleeMaterial = fleeMat;
    }

    public override void OnEnter()
    {
        if (fleeMaterial != null)
        {
            Instance.GhostRenderer.material = fleeMaterial;
            Instance.HeartRenderer.material = fleeMaterial;
            Instance.lEyeRenderer.material = fleeMaterial;
            Instance.rEyeRenderer.material = fleeMaterial;
        }
        else
        {
            Debug.LogError($"Ghost: {Instance.gameObject.name} has no Power Up Material assigned!");
        }

        if (Instance.Target != null)
        {
            target = Instance.Target.transform;
        }
        else
        {
            Instance.SetState(new GhostState_Idle(Instance));
        }
    }

    public override void OnUpdate()
    {
        if (Random.Range(0, 100000) < 20)
        {
            int x = Random.Range(0, (Instance.fleeBark.Length));
            Instance.audioSource.PlayOneShot(Instance.fleeBark[x]);
        }

        if (GameManager.Instance.PowerUpTimer == -1)
        {
            if (Vector3.Distance(Instance.transform.position, target.position) < Instance.Agent.stoppingDistance)
            {
                if (Instance.Target.Die() == true)
                {
                    Instance.SetState(new GhostState_Idle(Instance));
                }
            }
        }
        Vector3 dir = (Instance.transform.position - target.position).normalized * (Instance.Agent.stoppingDistance * 2);

        //Ray ray = new Ray(Instance.transform.position, dir);
        //if (Physics.Raycast(ray, out RaycastHit rayHit) == true)
        //{
            //Debug.DrawRay(Instance.transform.position, dir, Color.red);
        //}
        //else
        //{        
        NavMesh.SamplePosition(Instance.transform.position + dir, out NavMeshHit navMeshHit, (Instance.Agent.stoppingDistance * 2), NavMesh.AllAreas);
        Instance.Agent.SetDestination(navMeshHit.position);
        Debug.DrawRay(Instance.transform.position, dir, Color.magenta);
        //}
    }
}

public class GhostState_ClydeFlee : GhostState
{
    private Material fleeMaterial;
    private Transform target;

    public GhostState_ClydeFlee(Ghost instance) : base(instance)
    {
    }

    public GhostState_ClydeFlee(Ghost instance, Material fleeMat) : base(instance)
    {
        fleeMaterial = fleeMat;
    }

    public override void OnEnter()
    {

        //Debug.Log("Clyde is now in CLydeFlee state");
        if (fleeMaterial != null)
        {
            Instance.GhostRenderer.material = Instance.DefaultMaterial;
            Instance.HeartRenderer.material = Instance.DefaultHeartMat;
            Instance.lEyeRenderer.material = Instance.DefaultHeartMat;
            Instance.rEyeRenderer.material = Instance.DefaultHeartMat;
        }
        else
        {
            Debug.LogError($"Ghost: {Instance.gameObject.name} has no Power Up Material assigned!");
        }

        if (Instance.Target != null)
        {
            target = Instance.Target.transform;
        }
        else
        {
            Instance.SetState(new GhostState_Idle(Instance));
        }
    }

    public override void OnUpdate()
    {
        if (Random.Range(0, 100000) < 20)
        {
            int x = Random.Range(0, (Instance.fleeBark.Length));
            Instance.audioSource.PlayOneShot(Instance.fleeBark[x]);
        }

        if (GameManager.Instance.PowerUpTimer == -1)
        {
            if (Vector3.Distance(Instance.transform.position, target.position) < Instance.Agent.stoppingDistance)
            {
                if (Instance.Target.Die() == true)
                {
                    Instance.SetState(new GhostState_Idle(Instance));
                }
            }
        }
        Vector3 dir = (Instance.transform.position - target.position).normalized * (Instance.Agent.stoppingDistance * 2);

        //Ray ray = new Ray(Instance.transform.position, dir);
        //if (Physics.Raycast(ray, out RaycastHit rayHit) == true)
        //{
        //Debug.DrawRay(Instance.transform.position, dir, Color.red);
        //}
        //else
        //{        
        NavMesh.SamplePosition(Instance.transform.position + dir, out NavMeshHit navMeshHit, (Instance.Agent.stoppingDistance * 2), NavMesh.AllAreas);
        Instance.Agent.SetDestination(navMeshHit.position);
        Debug.DrawRay(Instance.transform.position, dir, Color.magenta);
        //}
    }
}

public class GhostState_Respawn : GhostState
{
    private Material respawnMaterial;
    private Vector3 target;

    public GhostState_Respawn(Ghost instance, Material respawnMat) : base(instance)
    {
        respawnMaterial = respawnMat;
    }

    public override void OnEnter()
    {
        Instance.SpawnBones();

        int x = Random.Range(0, Instance.dieBark.Length);
        Instance.audioSource.PlayOneShot(Instance.dieBark[x]);

        //switch off all objects in bone array
        for (int i = 0; i < Instance.boneRenderer.Length; i++)
        {
            Instance.boneRenderer[i].enabled = false;
        }

        if (respawnMaterial != null)
        {
            Instance.GhostRenderer.material = respawnMaterial;
            Instance.HeartRenderer.material = respawnMaterial;
            Instance.lEyeRenderer.material = respawnMaterial;
            Instance.rEyeRenderer.material = respawnMaterial;
        }
        else
        {
            Debug.LogError($"Ghost: {Instance.gameObject.name} has no Respawn Material assigned!");
        }
        target = GameManager.Instance.GhostSpawnBounds.center;
        Instance.Agent.speed = Instance.Agent.speed * 2;
        Instance.Agent.SetDestination(target);
    }

    public override void OnUpdate()
    {
        if (Vector3.Distance(Instance.transform.position, target) < Instance.Agent.stoppingDistance)
        {
            if (GameManager.Instance.PowerUpTimer != -1)
            {
                Instance.SetState(Instance.FleeState);
            }
            else //if state != default state
            {
                
                Instance.SetState(Instance.DefaultState);
            }
            //else perform attack animation,activate weapon hitbox
        }
    }

    public override void OnExit()
    {
        for (int i = 0; i < Instance.boneRenderer.Length; i++)
        {
            Instance.boneRenderer[i].enabled = true;
        }
        Instance.Agent.speed = Instance.Agent.speed / 2;
    }
}