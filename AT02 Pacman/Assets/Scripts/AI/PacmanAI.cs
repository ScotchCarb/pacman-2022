using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class PacmanAI : MonoBehaviour
{
    [SerializeField] Transform target;
    public NavMeshAgent Agent { get; private set; }

    private Animator anim;

    private void Awake()
    {
        TryGetComponent(out NavMeshAgent agent);
        if (agent != null)
        {
            Agent = agent;
        }
        else
        {
            Debug.LogError($"Ghost: {gameObject.name} must have a NavMesh Agent!");
        }

        TryGetComponent(out anim);
        anim.SetBool("running", true);

    }

    // Update is called once per frame
    void Update()
    {
       
    }
}
