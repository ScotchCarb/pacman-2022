using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class MainMenuGhost : MonoBehaviour
{
    [SerializeField] Transform[] target;
    public NavMeshAgent Agent { get; private set; }
    public int i = 0;


    private void Awake()
    {
        TryGetComponent(out NavMeshAgent agent);
        if (agent != null)
        {
            Agent = agent;
        }
        else
        {
            Debug.LogError($"Ghost: {gameObject.name} must have a NavMesh Agent!");
        }

    }

    // Update is called once per frame
    void Update()
    {
        if(Vector3.Distance(transform.position, target[i].position) < Agent.stoppingDistance * 2)
        {                
            i++;
            if(i == target.Length)
            {
                i = 0;
            }
        }

        Agent.SetDestination(target[i].position);
    }
}
