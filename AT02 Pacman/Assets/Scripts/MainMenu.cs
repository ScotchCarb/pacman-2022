using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;


public class MainMenu : MonoBehaviour
{
    [SerializeField] private GameObject optionsPanel;
    [SerializeField] private GameObject keyPanel;
    [SerializeField] private GameObject gamePanel;
    [SerializeField] private GameObject graphicPanel;
    [SerializeField] private GameObject soundPanel;


    private void Start()
    {
        optionsPanel.SetActive(false);
        keyPanel.SetActive(true);
        gamePanel.SetActive(false);
        graphicPanel.SetActive(false);
        soundPanel.SetActive(false);

        Cursor.visible = true;
        Cursor.lockState = CursorLockMode.Confined;
    }
    public void ChangeScene()
    {
        SceneManager.LoadScene("LevelOne");
    }

    public void Quit()
    {
        Application.Quit();
    }
}
