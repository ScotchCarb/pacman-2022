using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using UnityEngine.AI;

public class GameManager : MonoBehaviour
{
    //Serialized variables
    [SerializeField] private float powerUpTime = 10;
    [SerializeField] private Text scoreText;
    [SerializeField] private Text finalScoreText;
    [SerializeField] private Text gameoverScoreText;
    private GameObject[] bonusItemSpawn;
    [SerializeField] private Bounds ghostSpawnBounds;
    [SerializeField] private GameObject endPanel;
    [SerializeField] private GameObject vicPanel;
    [SerializeField] private AudioClip pelletClip;
    [SerializeField] private AudioClip powerPelletClip;
    [SerializeField] private AudioClip bonusItemClip;
    [SerializeField] private AudioClip eatGhostClip;

    [SerializeField] private InputField console;
    [SerializeField] private Text consoleText;
    [SerializeField] private Text consoleInput;

    //public NavMeshSurface surface;

    //Private variables
    private GameObject[] bonusItem;
    private int totalPellets = 0;
    private int score = 0;
    private int collectedPellets = 0;
    private int checkPellets = 0;
    private AudioSource aSrc;

    //Auto-properties
    public float PowerUpTimer { get; private set; } = -1;
    public Bounds GhostSpawnBounds { get { return ghostSpawnBounds; } }

    //Singletons
    public static GameManager Instance { get; private set; }

    //Delegates
    public delegate void PowerUp();
    public delegate void GameEvent();
    public GameEvent Delegate_GameOver = delegate { };
    public GameEvent Delegate_GameVictory = delegate { };

    //Events
    public event PowerUp Event_PickUpPowerPellet = delegate { };
    public event PowerUp Event_EndPowerUp = delegate { };
    public event GameEvent Event_GameVictory = delegate { };
    public event GameEvent Event_GameOver = delegate { };

    /// <summary>
    /// Create necessary references.
    /// </summary>
    private void Awake()
    {
        //Set singleton
        if(Instance == null)
        {
            Instance = this;
        }
        else
        {
            Debug.LogError("Game Manager: More than one Game Manager is present in the scene.");
            enabled = false;
        }
        //Set audio source reference
        TryGetComponent(out AudioSource audioSource);
        if(audioSource != null)
        {
            aSrc = audioSource;
        }
        else
        {
            Debug.LogError("Game Manager: No audio source attached to Game Manager!");
        }
        //Find bonus item
        
        bonusItem = GameObject.FindGameObjectsWithTag("Bonus Item");
        bonusItemSpawn = GameObject.FindGameObjectsWithTag("BonusSpawn");

        totalPellets = GameObject.FindGameObjectsWithTag("Pellet").Length;
        totalPellets += GameObject.FindGameObjectsWithTag("Power Pellet").Length;
    }

    /// <summary>
    /// Set initial game state.
    /// </summary>
    private void Start()
    {
        //set cursor to not be visible
        Cursor.visible = false;
        Cursor.lockState = CursorLockMode.Confined;


        //NavMeshBuilder.ClearAllNavMeshes();
        //NavMeshBuilder.BuildNavMesh();
        //surface.BuildNavMesh();

        //Assign delegates/events
        Event_GameVictory += ToggleVictoryPanel;
        Event_GameOver += ToggleEndPanel;

        Delegate_GameOver += ToggleEndPanel;
        Delegate_GameVictory += ToggleVictoryPanel;
        //Disable bonus item
        if (bonusItem != null)
        {
            for (int i = 0; i < bonusItem.Length; i++)
            {
                bonusItem[i].SetActive(false);
            }            
        }
        else
        {
            Debug.LogError("Game Manager: Bonus item must be in the scene and tagged as 'Bonus Item'!");
        }
        //Disable end game panel
        if (endPanel != null)
        {
            if (endPanel.activeSelf == true)
            {
                ToggleEndPanel();
            }
        }
        else
        {
            Debug.LogError("Game Manager: End Panel has not been assigned!");
        }
        //disable victory panel
        if (vicPanel != null)
        {
            if (vicPanel.activeSelf == true)
            {
                ToggleVictoryPanel();
            }
        }
        else
        {
            Debug.LogError("Game Manager: Victory Panel has not been assigned!");
        }
        //Set score text value
        if (scoreText != null)
        {
            scoreText.text = $"Score: {score}";
        }
        else
        {
            Debug.LogError("Game Manager: Score Text has not been assigned!");
        }
    }

    /// <summary>
    /// Frame by frame functionality.
    /// </summary>
    void Update()
    {
        //Active power up timer
        if(PowerUpTimer > -1)
        {
            PowerUpTimer += Time.deltaTime;
            if(PowerUpTimer > powerUpTime)  //Power up timer finished
            {
                Event_EndPowerUp.Invoke();
                PowerUpTimer = -1;
            }
        }

        if (Input.GetButtonDown("Debug"))
        {
            if (console.gameObject.activeSelf == false)
            {
                console.gameObject.SetActive(true);
                console.Select();
                Time.timeScale = 0;
                Cursor.visible = true;
            }
            else
            {
                console.gameObject.SetActive(false);
                Time.timeScale = 1;
                Cursor.visible = false;
            }
        }
    }


    /// <summary>
    /// Called when a pellet is picked up.
    /// </summary>
    /// <param name="value"></param>
    /// <param name="powerUp"></param>
    /// <param name="bonus"></param>
    public void PickUpPellet(int value, int type = 0)
    {
        //Add score
        score += value;
        //Set score text value
        if (scoreText != null)
        {
            scoreText.text = $"Score: {score}";
        }
        else
        {
            Debug.LogError("Game Manager: Score Text has not been assigned!");
        }

        if(type == 0)
        {
            aSrc.PlayOneShot(pelletClip);
        }
        else if (type == 1) //Activate power up
        {
            Event_PickUpPowerPellet.Invoke();
            PowerUpTimer = 0;
            aSrc.PlayOneShot(powerPelletClip);
        }

        if (type != 2)
        {
            collectedPellets++;
            checkPellets++;
            //Check ratio of collected pellets
            float ratio = (float)collectedPellets / totalPellets;
            if (ratio != 1)
            {
                float _ratio = ratio % 0.25f;
                
                if (checkPellets == 10)
                {
                    int chance = Random.Range(1, 100);
                    if (chance <= 10)
                    {
                        //Spawn in bonus item
                        if (bonusItem != null)
                        {

                            int x = Random.Range(0, bonusItem.Length);
                            if (bonusItem[x].activeSelf == false)
                            {

                                if (bonusItemSpawn != null)
                                {

                                    int y = Random.Range(0, bonusItemSpawn.Length);
                                    bonusItem[x].transform.parent = bonusItemSpawn[y].transform;
                                    bonusItem[x].transform.position = bonusItemSpawn[y].transform.position;
                                    bonusItem[x].SetActive(true);
                                }
                                else
                                {
                                    Debug.LogError("Game Manager: Bonus Item Spawn has not been assigned!");
                                }
                            }
                        }
                        else
                        {
                            Debug.LogError("Game Manager: Bonus item must be in the scene and tagged as 'Bonus Item'!");
                        }
                    }
                }
            }
            else
            {
                Event_GameVictory.Invoke();
            }
        }
        else
        {
            aSrc.PlayOneShot(bonusItemClip);
        }
    }

    /// <summary>
    /// Called when a ghost is eaten.
    /// </summary>
    /// <param name="ghost"></param>
    public void EatGhost(Ghost ghost)
    {
        //Add score
        score += 5;
        //Set score text value
        if (scoreText != null)
        {
            scoreText.text = $"Score: {score}";
        }
        aSrc.PlayOneShot(eatGhostClip);
        //Respawn
        ghost.SetState(ghost.RespawnState);
    }

    /// <summary>
    /// Resets the scene.
    /// </summary>
    public void RestartGame()
    {
        Time.timeScale = 1;
        SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);
    }

    /// <summary>
    /// Quits the game.
    /// </summary>
    public void QuitToDesktop()
    {
        Application.Quit();
    }

    /// <summary>
    /// Toggles the end game panel on and off.
    /// </summary>
    private void ToggleEndPanel()
    {
        Instance.gameoverScoreText.text = "Your Score: " + Instance.score;
        if (endPanel.activeSelf == false)
        {
            Time.timeScale = 0;
            Cursor.visible = true;
            endPanel.SetActive(true);
        }
        else
        {
            Time.timeScale = 1;
            Cursor.visible = false;
            endPanel.SetActive(false);
        }
    }

    private void ToggleVictoryPanel()
    {
        Instance.finalScoreText.text = "Your Score: " + Instance.score;
        if (vicPanel.activeSelf == false)
        {
            Time.timeScale = 0;
            Cursor.visible=true;
            vicPanel.SetActive(true);
        }
        else
        {
            Time.timeScale = 1;
            Cursor.visible=false;
            vicPanel.SetActive(false);
        }
    }

    private void OnDrawGizmos()
    {
        Gizmos.color = Color.red;
        Gizmos.DrawWireCube(ghostSpawnBounds.center, ghostSpawnBounds.size);
    }

    //commands for debugging and testing purposes
    public void PrintDebug()
    {
        string input = consoleInput.text;

        
        if (input == "powerup")
        {
            Event_PickUpPowerPellet.Invoke();
            powerUpTime = 10000000;
            PowerUpTimer = 0;
            consoleText.text += "Power Up granted \n";            
        }
        else if(input == "powerdown")
        {
            PowerUpTimer = -1;
            powerUpTime = 10;
            consoleText.text += "Power Up removed \n";
            
        }
        else if (input == "vicpanel")
        {
            ToggleVictoryPanel();
        }
        else if(input == "endpanel")
        {
            ToggleEndPanel();
        }
        else
        {
            consoleText.text += "Invalid input \n";            
        }
    }
}
