using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class PauseMenu : MonoBehaviour
{
    [SerializeField] private string nextScene;
    [SerializeField] private GameObject pauseMenu;
    [SerializeField] private GameObject optionsMenu;
    [SerializeField] private GameObject mapPanel;
    [SerializeField] private GameObject endPanel;
    [SerializeField] private GameObject vicPanel;
    [SerializeField] private GameObject keyPanel;
    [SerializeField] private GameObject graphicsPanel;
    [SerializeField] private GameObject soundPanel;
    [SerializeField] private GameObject quitPanel;
    [SerializeField] private GameObject mainPanel;
    
    //auto properties
    

    private bool paused;

    private void Awake()
    {
        paused = false;
        pauseMenu.SetActive(false);
        optionsMenu.SetActive(false);
        mapPanel.SetActive(false);
        endPanel.SetActive(false);
        vicPanel.SetActive(false);
        keyPanel.SetActive(true);
        graphicsPanel.SetActive(false);
        soundPanel.SetActive(false);
        quitPanel.SetActive(false);
        mainPanel.SetActive(false);
    }

    private void Update()
    {
        if(Input.GetButtonDown("Pause") && paused == false)
        {
            Cursor.visible = true;            
            paused = true;
            PauseGame(true);
        }
        else if(Input.GetButtonDown("Pause") && paused == true)
        {
            Cursor.visible = false;
            paused = false;
            PauseGame(false);
        }
    }

    public void PauseGame(bool pause)
    {
        if(pause == true)
        {
            //pause the game
            Time.timeScale = 0;
            //open the pause menu
            pauseMenu.SetActive(true);
        }
        else
        {
            //unpause the game
            Time.timeScale = 1;
            //close the pause menu
            pauseMenu.SetActive(false);
            optionsMenu.SetActive(false);
            Cursor.visible = false;
        }
    }

    public void ChangeScene(string scene)
    {
        PauseGame(false);
        paused = false;
        SceneManager.LoadScene(scene);
    }

    public void QuitGame()
    {
        Application.Quit();
    }
}
