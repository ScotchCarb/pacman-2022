using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

//script by Ian R Bell, last update 23/3/2022
public class Interaction : MonoBehaviour
{
    //variables to control distance and radius of the SphereCast for interacting with objects
    [SerializeField] private float distance = 0.7f;
    //[SerializeField] private float radius = 2f;
    //game object reference for the UI object to display when the player is standing in front of something they can interact with
    [SerializeField] public GameObject promptUI;

    //variable for the instance of this script
    public static Interaction Instance { get; private set; }

    private void Awake()
    {
        //singleton setup
        if (Instance == null)
        {
            Instance = this;
        }
        else
        {
            enabled = false;
        }
    }

    void Update()
    {if (Input.GetButtonDown("Interact")) //if player hits the interaction button
                {
        //check if player is facing an interaction object and within range of interaction object
        if (Physics.Raycast(transform.position, transform.forward, out RaycastHit hit, distance) == true)
        {
            Debug.DrawRay(transform.position, transform.forward * distance, Color.red);
            //display UI element 'InteractionPrompt'
            if (hit.collider.TryGetComponent(out IPrompt prompt) == true)
            {
                //display the prompt text
                if (hit.collider.TryGetComponent(out IButton button) == true)
                {
                    button.Prompt();
                }
                // ignore this section for now - later I may add text which appears to show the player what the interaction will do
                //promptUI.GetComponent<PromptText>().Activate();
                //promptUI.GetComponent<PromptText>().promptObject = hit.transform;
                //promptUI.GetComponent<PromptText>().promptText.text = hit.collider.GetComponent<IPrompt>().Prompt();
                                
                    //check what kind of interaction the item has through TryGetComponent
                    if (hit.collider.TryGetComponent(out IButton door) == true)
                    {
                        door.Open();
                    }
                    if (hit.collider.TryGetComponent(out IKey key) == true)
                    {
                        key.Pickup();
                    }
                }

            }
            /* ignore until prompt stuff is implemented
            else
            {
                if (promptUI.activeSelf == true)
                {
                    //deactivate the prompt text
                    //again, disregard for now until interaciton prompt stuff has been implemented
                    //promptUI.GetComponent<PromptText>().Deactivate(); 
                }
            }
            */
        }
    }

    
}



//interfaces for various item types that can be interacted with
public interface IPrompt
{
    string Prompt();
}

public interface IButton : IPrompt
{
    void Open();
}

public interface IKey : IPrompt
{
    void Pickup();
}
