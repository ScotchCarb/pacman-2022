using System.Collections;
using System.Collections.Generic;
using UnityEngine;

//Script by Ian R Bell, last updated 23/3/2022
public class ButtonScript : MonoBehaviour, IButton
{
    //references to the door object this button is for and DoorScript attached to that object
    [SerializeField] GameObject door;
    [SerializeField] GameObject btnIndicator;
    [SerializeField] DoorScript doorScript;

    //reference to the animator on the button
    private Animator btnAnim;

    private void Awake()
    {

        if(btnIndicator != null)
        {
            btnIndicator.SetActive(false);
        }

        //auto property setup for doorScript
        door.TryGetComponent(out doorScript);

        //setting up the animator for the button
        if(btnAnim == null)
        {
            GetComponentInChildren<Animator>().TryGetComponent(out btnAnim);
        }
    }
   //method called when the player interacts with the object
    public void Open()
    {
        doorScript.Open();
        btnAnim.SetTrigger("pulled");
    }
    //unused code which returns text to display as a prompt in the game space when character is in range to interact with it 
    public string Prompt()
    {
        string doorString = "You Stand Before A Door";
        return doorString;
    }

    private void OnTriggerEnter(Collider other)
    {
        if(other.tag == "Player")
        {
            btnIndicator.SetActive(true);
        }
    }

    private void OnTriggerExit(Collider other)
    {
        if (other.tag == "Player")
        {
            btnIndicator.SetActive(false);
        }

    }
}
