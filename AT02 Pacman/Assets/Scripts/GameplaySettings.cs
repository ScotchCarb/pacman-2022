using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using System.Runtime.Serialization.Formatters.Binary;
using System.IO;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class GameplaySettings : MonoBehaviour
{
    //dropdown component
    [SerializeField] private Dropdown diffSet;

    private void Awake()
    {
        if(!File.Exists(Application.persistentDataPath + "/GameData.dat"))
        {
            Debug.LogWarning("No file found, setting to default");
            diffSet.value = 2;
            SetDifficulty();
        }
        else
        {            
            LoadSettings();
        }
    }

    public void SetDifficulty()
    {
        int diffIndex = diffSet.value;

        if (diffIndex == 0)
        {
            ChoseJournalist();
        }
        else
        {
            BinaryFormatter diff = new BinaryFormatter();
            FileStream file = File.Create(Application.persistentDataPath + "/GameData.dat");
            GameData data = new GameData();
            data.SaveDiff = diffIndex;
            diff.Serialize(file, data);
            file.Close();
        }
    }

    public void LoadSettings()
    {
        Debug.Log("Loading settings");
        BinaryFormatter data = new BinaryFormatter();
        FileStream file = File.Open(Application.persistentDataPath + "/GameData.dat", FileMode.Open);
        GameData gameData = (GameData)data.Deserialize(file);
        file.Close();
        diffSet.value = gameData.SaveDiff;
    }

    public void ChoseJournalist()
    {
        SceneManager.LoadScene(2);
        
        //Application.Quit();
    }
}

//constructor for the selected difficulty  
[Serializable]
  class GameData
    {
    [SerializeField]
    private int saveDiff;
    public int SaveDiff { get { return saveDiff; } set { saveDiff = value; } }
    }