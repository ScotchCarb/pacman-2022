using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class BasicAI_Nav : MonoBehaviour
{
    //navagent target
    [SerializeField] Transform target;

    public NavMeshAgent agent;
    private Transform actualTarget;

    //
    private void Awake()
    {
        if(TryGetComponent(out agent) == true)
        {
            agent.SetDestination(target.position);
        } //assign navmeshagent component to variable
    }

    void Start()
    {
        
       
    }

    void Update()
    {
        Debug.DrawRay(transform.position, transform.forward * 4, Color.blue);
        if (target != null)
        {
            //set destination to target
            agent.SetDestination(target.position);
            if(Physics.Raycast(transform.position, transform.forward, out RaycastHit hit, 4) == true && target.tag != "Door")
            {
                Debug.Log(hit);
                Transform parent = hit.transform.parent;
                List<Transform> doors = new List<Transform>();
                if(hit.transform.parent != null)
                {
                    for(int i = 0; i < parent.childCount; i++)
                    {
                        //check to see if child is a door
                        if(parent.GetChild(i).tag == "Door")
                        {
                            doors.Add(parent.GetChild(i));
                        }
                    }
                }

                Transform closestDoor = null;
                //this for loop finds the closest door
                for(int i = 0; i < doors.Count; i++)
                {
                    if (closestDoor != null)
                    {
                        if (Vector3.Distance(transform.position, doors[i].position) < Vector3.Distance(transform.position, closestDoor.position))
                        {
                            closestDoor = doors[i]; 
                        }
                    }
                    else
                    {
                        closestDoor = doors[i];
                    }
                }
                actualTarget = target;
                target = closestDoor;
            }
            if (Vector3.Distance(transform.position, target.position) <= agent.stoppingDistance)
            {
                if(actualTarget != null)
                {
                    target = actualTarget;
                    actualTarget = null;
                }
                agent.isStopped = true;
            }
            else
            {
                agent.SetDestination(target.position);
                agent.isStopped = false;
            }
        }
    }
}
